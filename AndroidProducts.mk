#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_java.mk

COMMON_LUNCH_CHOICES := \
    lineage_java-user \
    lineage_java-userdebug \
    lineage_java-eng
